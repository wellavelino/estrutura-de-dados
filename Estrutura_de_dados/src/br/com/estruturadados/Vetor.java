package br.com.estruturadados;

import java.util.Arrays;

import br.com.estruturadados.bean.Aluno;

public class Vetor
{
    private Aluno[] alunos = new Aluno[100];

    public void adicionaAluno(Aluno aluno)
    {
    }

    public void adicionaAluno(int posicao, Aluno aluno)
    {
    }

    public Aluno obterAluno(int posicao)
    {
	return null;
    }

    public void removeAluno(int posicao)
    {
    }

    public boolean contemAluno(Aluno aluno)
    {
	return false;
    }

    public int tamanho()
    {
	return 0;
    }

    public String toString()
    {
	return Arrays.toString(alunos);
    }
}
