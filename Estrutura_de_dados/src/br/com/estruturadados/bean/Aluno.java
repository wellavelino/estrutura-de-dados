package br.com.estruturadados.bean;

public class Aluno
{
    private String nome;

    public String getNome()
    {
	return nome;
    }

    public void setNome(String nome)
    {
	this.nome = nome;
    }

    /* to string para realizar a impress�o de valores */
    public String toString()
    {
	return this.nome;
    }

    /* Metodo para comparar objetos Aluno entre si */
    public boolean equals(Object o)
    {
	Aluno outro = (Aluno) o;
	return this.nome.equals(outro.nome);
    }
}
